import sys
import tkinter as tk
import tkinter.messagebox
import json
import asyncio
import websockets

class HangmanGUI:
    def __init__(self, master):
        self.master = master
        self.master.title("Hangman")
        self.master.geometry("900x500")  # Adjust the window size
        self.master.configure(bg="black")
        # Fonts
        self.title_font = ("Palatino Linotype", 20, "bold")
        self.label_font = ("Palatino Linotype", 12)
        self.button_font = ("Palatino Linotype", 12)

        # Variables
        self.lives = 6
        self.word = ""
        self.hint = ""
        self.guessed = []
        self.second_hint = ""
        self.gameover = False
        self.win = False

        # Widgets
        self.title_label = tk.Label(master, text="Hangman", font=self.title_font, bg="black", fg="white")
        self.title_label.place(relx=0.5, rely=0.1, anchor="center")

        self.word_label = tk.Label(master, text="", font=self.label_font, bg="black", fg="white")
        self.word_label.place(relx=0.5, rely=0.2, anchor="center")

        self.guessed_label = tk.Label(master, text="Guessed: {}".format(self.guessed), font=self.label_font, bg="black", fg="white")
        self.guessed_label.place(relx=0.5, rely=0.3, anchor="center")

        self.hint_label = tk.Label(master, text="Hint: {}".format(self.hint), font=self.label_font, bg="black", fg="white")
        self.hint_label.place(relx=0.5, rely=0.4, anchor="center")

        self.lives_label = tk.Label(master, text="Lives: {}".format(self.lives), font=self.label_font, bg="black", fg="white")
        self.lives_label.place(relx=0.5, rely=0.5, anchor="center")

        self.play_button = tk.Button(master, text="Play", font=self.button_font, command=self.start_game)
        self.play_button.place(relx=0.5, rely=0.6, anchor="center", width=200)

        self.quit_button = tk.Button(master, text="Quit", font=self.button_font, command=master.destroy)
        self.quit_button.place(relx=0.5, rely=0.7, anchor="center", width=200)

        self.guess_entry = tk.Entry(master, font=self.label_font)
        self.guess_entry.place(relx=0.5, rely=0.8, anchor="center", width=200)

        self.guess_button = tk.Button(master, text="Guess", font=self.button_font, command=self.make_guess)

    def start_game(self):
        if len(sys.argv) != 3:
            print("Usage: python hangmanclient.py <address> <port>")
            return

        address = sys.argv[1]
        port = sys.argv[2]

        async def play():
            async for websocket in websockets.connect("ws://{}:{}".format(address, port)):
                print("Hangman")
                self.lives_label.config(text="Lives: {}".format(self.lives))

                # wait for the server to send the word and the hint
                event = await websocket.recv()
                data = json.loads(event)

                self.word = data["word"]
                self.hint = data["hint"]
                self.guessed = []
                self.second_hint = ""
                self.gameover = False
                self.win = False

                self.word_label.config(text="The word has {} letters".format(len(self.word)))
                self.hint_label.config(text="Hint: {}".format(self.hint))

                self.guess_entry.place(relx=0.5, rely=0.8, anchor="center")

                while True:
                    if self.gameover:
                        self.guess_entry.place_forget()
                        self.guess_button.place_forget()
                        break

                    letter = await self.get_letter_input()
                    if len(letter) != 1:
                        tk.messagebox.showerror("Error", "Please enter a single letter")
                        continue
                    await websocket.send(json.dumps({"letter": letter}))
                    event = await websocket.recv()
                    data = json.loads(event)
                    if data["type"] == "guess":
                        guessed_str = "".join(letter if letter in data["guessed"] else "_" for letter in self.word)
                        self.word_label.config(text="Word: {}".format(guessed_str))
                        self.guessed_label.config(text="Guessed: {}".format(data["guessed"]))
                        self.lives_label.config(text="Lives: {}".format(data["lives"]))
                    # check if second hint is in the data
                    if "second_hint" in data:
                        self.second_hint = data["second_hint"]
                        self.hint_label.config(text="Hint: {}".format(self.hint + "\nSecond Hint: " + self.second_hint))
                    if data["gameover"]:
                        event = await websocket.recv()
                        data = json.loads(event)
                        if data["type"] == "gameover":
                            self.gameover = True
                            self.win = data["win"]
                            break
                if self.gameover:
                    if self.win:
                        tk.messagebox.showinfo("Congratulations", "You win!")
                    else:
                        tk.messagebox.showinfo("Game Over", "You lose! The word was {}".format(self.word))
                    self.guessed = []
                    self.word_label.config(text="")
                    self.guessed_label.config(text="")
                    self.lives_label.config(text="Lives: {}".format(self.lives))

        asyncio.get_event_loop().run_until_complete(play())

    async def get_letter_input(self):
        while True:
            await asyncio.sleep(0.1)
            self.master.update()  # Update the GUI to prevent it from freezing
            if self.guess_entry.get():
                letter = self.guess_entry.get()[0]
                self.guess_entry.delete(0, tk.END)
                return letter

    def make_guess(self):
        letter = self.guess_entry.get()
        if len(letter) != 1:
            tk.messagebox.showerror("Error", "Please enter a single letter")
            return
        self.guess_entry.delete(0, tk.END)

root = tk.Tk()
my_gui = HangmanGUI(root)
root.mainloop()
